<?php require_once("../includes/initialize.php"); ?>
<?php
if (!$session->is_logged_in()) { redirect_to("index.php"); }
 ?>
<?php
	// must have an ID
  if(empty($_GET['id'])) {
  	$session->message("No Ad ID was provided.");
    redirect_to('index.php');
  }

  $ad = Ad::find_by_id($_GET['id']);
  if ($session->author === $ad->author) {
      if($ad && $ad->delete()) {
          $session->message("The Ad was deleted.");
          redirect_to('index.php');
      }
  } else {
    $session->message("The Ad could not be deleted.");
    redirect_to('index.php');
  }
  
?>
<?php if(isset($database)) { $database->close_connection(); } ?>
