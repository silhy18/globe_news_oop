<footer>
  &copy; <?php echo date('Y'); ?> <h6><a href="index.php">Globe News</a></h6>
</footer>
<script src="/globe_news_oop/public/js/bootstrap.bundle.js"></script>
<script src="/globe_news_oop/public/js/jquery.js"></script>
<script src="/globe_news_oop/public/js/jquery.validate.js"></script>
<script src="/globe_news_oop/public/js/form.js"></script>
</body>
</html>
<?php if(isset($database)) { $database->close_connection(); } ?>