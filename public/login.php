<?php
require_once("../includes/initialize.php");

if($session->is_logged_in()) {
  redirect_to("index.php");
}

// Remember to give your form's submit tag a name="submit" attribute!
if (isset($_POST['submit'])) { // Form has been submitted.

  $username = trim($_POST['username']);
  $password = trim($_POST['password']);
  
    $sql = "SELECT * FROM users ";
    $sql .= "WHERE username='$username'";
    $sql .= "LIMIT 1";
    $result = $database->query($sql);
    $database->confirm_query($result);
    $found_user = mysqli_fetch_assoc($result); // find first
    mysqli_free_result($result);

   if(password_verify($password, $found_user['hashed_password'])) {
	    session_regenerate_id();
	    $session->login($username);
	    $_SESSION['user_id'] = $found_user['id'];
	    $_SESSION['author'] = $found_user['username'];
	    $_SESSION['message'] = "Logged in successfully.";
        redirect_to("index.php");
	} else {
	    // username/password combo was not found in the database
       $_SESSION['message'] = "Username/password combination incorrect.";
       redirect_to("index.php");
	}
}
?>