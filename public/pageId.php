<?php
require_once('../includes/initialize.php');
if (!$session->is_logged_in()) { redirect_to("index.php"); }

if(is_post_request()) {
		$ad = new Ad();
		$ad->title = $_POST['title'];
	   $ad->description = $_POST['description'];
	   $ad->author = $_POST['author'];
		$ad->created_at = $_POST['created_at'];
		
	   if($ad->is_unique($ad->title)) {
		    if($ad->save()) {
			// Success
          $_SESSION['message'] = "Ad created successfully.";
	     //	redirect_to('index.php');
	       }
	    } else {
	      $_SESSION['message'] = "Ad with this title already exists.";
	      $_SESSION['title'] = $_POST['title'];
	      $_SESSION['description'] = $_POST['description'];
	    //  redirect_to('create.php');
	    }
	
		   $new_id = $database->insert_id($database->connection);
		
	     	$return = array('success' => false); 
          if($new_id) { 
              $return = array('success' => true, 'id' => $new_id); 
          } 
        echo json_encode($return);
		}

?>